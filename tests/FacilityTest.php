<?php
namespace TDD\Test;

require dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use PHPUnit\Framework\TestCase;

use TDD\Facility;

class FacilityTest extends TestCase {
    
    public function testFacilityClass() {

        $Facility = new Facility();
        $this->assertEquals(1, $Facility->getId(), "The hardcoded value of 1 for the id should match" );
      

    }

}
